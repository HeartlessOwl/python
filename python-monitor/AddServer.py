import pickle
from CheckServer import Server

servers = pickle.load( open( "servers.pickle", "rb" ) )

print(" Please Add A Server")

servername = input("Enter The Name")
port = int(input("Enter A Port Number"))
connection = input("Connection Type (ping, plain, ssl)")
priority = input("Enter Priority (High/Low)") #For the Emailing Purposes

new_server = Server(servername, port, connection, priority)
servers.append(new_server)

pickle.dump(servers, open("servers.pickle", "wb" ) )